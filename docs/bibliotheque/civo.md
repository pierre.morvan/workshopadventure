# CIVO

## Ajouter la completion à bash
```sh
apt-get install bash-completion -y
source /usr/share/bash-completion/bash_completion
echo 'source <(civo completion bash)' >>~/.bashrc
source ~/.bashrc
```

## Mettre un alias à la commande civo
```sh
echo 'alias c=civo' >>~/.bashrc
echo 'complete -F __start_civo c' >>~/.bashrc
source ~/.bashrc
```

## Ajouter une clef d'API pour pouvoir utiliser la CLI
```sh
civo apikey save <alias de la clef> <clef d'api>
```

## Créer un cluster kubernetes
```sh
civo k3s create <nom du cluster> --size "g4s.kube.medium" --nodes 2 --wait --save --yes
```

## Lister les cluster existants
```sh
civo k3s ls
```

## Supprimer un cluster existant
```sh
civo k3s rm <nom du cluster>
```

## Lister les firewall existants
```sh
civo firewall ls
```

## Supprimer un firewall
```sh
civo firewall rm <nom du firewall>
```

## Créer un firewall (en utilisant curl directement)
```sh
# Dans l'exemple le firewall est 'kube-firewall' pour une exposition de uniquement les ports 80,443,6443
CIVO_APIKEY=$(jq -r '.apikeys.gitops' $HOME/.civo.json)
CIVO_NETWORK_REGION=$(jq -r '.meta.default_region' $HOME/.civo.json)
CIVO_URL=$(jq -r '.meta.url' $HOME/.civo.json)

EXISTING_FIREWALL=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.name == "kube-firewall")')
if [ "$EXISTING_FIREWALL" == "" ]
then
    echo "Creating civo firewall."

    CIVO_NETWORK_ID=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/networks?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.default) | .id')

    curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls" \
         -d "name=kube-firewall&network_id=$CIVO_NETWORK_ID&region=$CIVO_NETWORK_REGION"
    FIREWALL_ID=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.name == "kube-firewall") | .id')
    FIREWALL_RULES=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls/$FIREWALL_ID/rules?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.direction == "ingress") | .id')
    echo "Deleting default rules."
    for rule in ${FIREWALL_RULES}
    do
        curl -s -X DELETE --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls/$FIREWALL_ID/rules/$rule?region=$CIVO_NETWORK_REGION"
    done
    echo "Create k8s rules."
    curl -s -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls/$FIREWALL_ID/rules?region=$CIVO_NETWORK_REGION" \
         -d "region=$CIVO_NETWORK_REGION&protocol=tcp&start_port=80,443,6443"
fi

```
