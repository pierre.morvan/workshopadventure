# Kubectl

## Aliases utiles pour kubectl
```sh
echo 'alias k=kubectl' >>~/.bashrc
echo 'alias kg="kubectl get"' >>~/.bashrc
echo 'complete -o default -F __start_kubectl k' >>~/.bashrc
source ~/.bashrc
```

**Note** Vous pouvez aussi utiliser des bibliothèques d'alias existantes comme https://github.com/ahmetb/kubectl-aliases

## Ajouter l'auto-completion de kubectl
```sh
echo 'source <(kubectl completion bash)' >>~/.bashrc
source ~/.bashrc
```
