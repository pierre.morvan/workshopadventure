### Utilisation de dhall dans les runner gitlab
Votre consultant Beornide Dhall vous fournit une image docker capable de gérer du dhall. Il vous suffit donc d'utiliser l'image suivante pour les runners : louiznk/k8s-tools:1.1.0 (au lieu de louiznk/k8s-tools:1.0.0). Ce runner a dhall installé avec son cache déjà préchargé avec tous les types utilisés dans les descripteurs fournis.  
Vous pouvez ensuite lancer les commandes dhall pour compiler les fichiers dhall en manifest kubernetes que vous déploierez ensuite avec la commande.

```shell
dhall-to-yaml --documents --file mon_fichier_de_composition.dhall | kubectl apply -f -
```

Ce qui donnerait dans le fichier .gitlab-ci.yaml
```yaml
# ...

## pour utiliser dhall il faut dans ".kubeconfig" utiliser l'image louiznk/k8s-tools:1.1.0
dhall-dev:
  stage: deploy-dhall-exemple
  extends: .kubeconfig
  script: 
    - echo "👋 Deploy dhall dans le namespace dhall-exemple"
    - kubectl create ns dhall-exemple 2>/dev/null || true
    - dhall-to-yaml --documents --file ./que-dalle/assembly-exemple.dhall | kubectl apply -n dhall-exemple -f -

# ...
```

À vous de modifier le fichier .gilab-ci.yaml pour intégrer cela (en modifiant les path, les noms des stages et ...).

**WARNING**
Ne pas gérer les différences entre environnements via les branches, c'est un antipattern.
