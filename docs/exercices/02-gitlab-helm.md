### Utilisation de heml dans les runner gitlab

Vous allez intégrer dans CI/CD les instructions pour déployer du helm en utilisant les manifests générés par helm (au lieu de la commande d'installation que fournit aussi helm) et en spécifiant le fichier de valeurs à utiliser en fonction de l'environnement à déployer.
La commande à passer est par exemple pour déployer le chart helm dans le dossier `enfer`

```shell
helm template exemple --create-namespace -n helm-exemple -f values-exemple.yaml --set testConnection=false ./enfer | kubectl apply -n helm-dev -f -
```

Ce qui donnerait dans le fichier .gitlab-ci.yaml
```yaml
# ...

helm-dev:
  stage: deploy-helm-exemple
  extends: .kubeconfig
  script: 
    - echo "👋 Deploy helm dans le namespace helm-exemple"
    - kubectl create ns helm-exemple 2>/dev/null || true
    - helm template exemple --create-namespace -n helm-exemple -f ./enfer/values-exemple.yaml --set testConnection=false ./enfer | kubectl apply -n helm-exemple -f -

# ...
```

À vous de modifier le fichier .gitlab-ci.yaml pour intégrer cela (en modifiant les path, les noms des stages et ...).

**WARNING**
Ne pas gérer les différences entre environnements via les branches, c'est un antipattern.
