## Usage avec kustomize

kustomize introduit une notion de surcharge via des descripteurs qui sont dans un chemin. Tous les env sont décrits via cette notion de surcharge. Il n'y a pas de templating, mais l'on peut pour certains besoins spécifiques (version des images, injection de secret, ...) avoir des `generator` qui vont faire cela (mais ce n'est pas abordé ici).


Chaque `overlays` (qui correspondent à nos envs) indique le dossier de ressources (`base`) qu'il va patcher.
Et l'on trouve dans chaque dossier de ressource un fichier `kustomiziation.yml` qui spécifie ce qui devra être fait.

```
	├── base
	│   ├── deployment.yml
	│   ├── ingress.yml
	│   ├── kustomization.yml
	│   └── svc.yml
	└── overlays
	    ├── moria
	    │   ├── patch.yml
	    │   └── kustomization.yml
	    ├── erebor
	    │   ├──
		...
```

Pour utiliser un `overlay` particulier, par exemple celui de la moria que l'on déploierait dans le namespace dwarf (en étant positionné à la racine du projet) :  

```shell
kustomize build ./overlays/moria/ | kubectl apply -n dwarf -f -
```

**WARNING**
Ne pas gérer les différences entre environnements via les branches, c'est un antipattern.
