## Usage avec Helm

L'approche de helm est basée sur du templating qui permet d'injecter les différences, soit via un fichier de valeurs (généralement nommé `values.yaml` et que l'on peut spécialiser par environnement `values-demo.yaml`...).


Avec en particulier `spec.source.helm.valueFiles` qui permet de passer un ou plusieurs fichiers de "value".

La stratégie utilisée ici avec helm est de générer les manifests kubernetes (sans les installer avec helm) en spécifiant le fichier contenant les valeurs à surcharger.  

Par exemple on déploiera la release "exemple-app" du chart helm qui est dans dossier "./enfer" dans le namespace (kubernetes) en surchargeant les valeurs par défaut avec le fichier values-env-exemple.yaml de la manière suivante :

```shell
helm template exemple-app --create-namespace -n exemple -f ./enfer/values-env-exemple.yaml --set testConnection=false ./enfer | kubectl apply -n exemple -f -
```

