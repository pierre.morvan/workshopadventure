## Usage avec kustomize dans ArgoCD

ArgoCD gère les `overlays` de kustomize en s'appuyant sur la définition du `spec.source.path` (comme pour un projet classique, rien de spécifique). Il détecte la présence du fichier de `kustomization` et utilise automatiquement la commande adéquate, par exemple ce descripteur déploiera (et surveillera) l'overlay ./overlays/demo dans le namespace test (qui sera créé si besoin).

```yaml
---
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: hello-world
  namespace: argocd
spec:
  project: default 
  destination:
    namespace: test
    server: https://kubernetes.default.svc
  source:
    path: 02-kustomize/overlays/demo
    repoURL: https://gitlab.com/gitops-heros/deploy-sith-from-gitlab.git
    targetRevision: main
  syncPolicy:
    automated:
      prune: true
      selfHeal: true 
    syncOptions:
    - CreateNamespace=true 
```

**WARNING**
Ne pas gérer les différences entre environnements via les branches, c'est un antipattern.


Vous allez intégrer dans CI/CD les instructions pour deployer du kustomize en spécifiant le dossier d'`overlay` qui correspond à l'environnement à déployer.  

Pour utiliser un `overlay` particulier, par exemple celui de la moria que l'on déploierait dans le namespace dwarf (en étant positionné à la racine du projet) :  

```shell
kustomize build ./overlays/moria/ | kubectl apply -n dwarf -f -
```

Ce qui donnerait dans le fichier .gitlab-ci.yaml
```yaml
# ...

helm-dev:
  stage: deploy-helm-exemple
  extends: .kubeconfig
  script: 
    - echo "👋 Deploy helm dans le namespace dwarf"
    - kubectl create ns dwarf 2>/dev/null || true
    - kustomize build ./overlays/staging/ | kubectl apply -n dwarf -f -

# ...
```

À vous de modifier le fichier .gitlab-ci.yaml pour intégrer cela (en modifiant les path, les noms des stages et ...).

**WARNING**
Ne pas gérer les différences entre environnements via les branches, c'est un antipattern.
