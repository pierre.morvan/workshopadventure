# Installation d'un cluster civo

## Instancier le projet de travail
Pour cette adventure votre livre de sort qui vous permettra de lancer et piloter votre infrastructure sera dans un worspace gitpod.

La première étape consiste à faire un fork du projet gitlab contenant tous les sorts et autres potions indispensables à tout aventurier du royaume de kubernetes.  
- Authentifiez vous sur [gitlab](https://www.gitlab.com)
- Optionnel: vous pouvez créer un [groupe](https://gitlab.com/groups/new) où seront vos 2 projets utiles à l'aventure.
- Allez sur le projet de [livre de sort](https://gitlab.com/gitops-heros/workshopadventure), faites en un [fork](https://gitlab.com/gitops-heros/workshopadventure/-/forks/new) (le lien est en haut à droite)
- Ouvrez VOTRE fork du projet dans Gitpod via le bouton **Gipod** situé en haut à droite, un peu en dessous de fork  (attention la combo box peut être sur "Web IDE", en ce cas changer le choix de la combo box)

## Configurer la cmd civo
Une fois le projet GitPod démarrer (c'est un peu plus long à la première instanciation) positionnez vous dans le shell en bas du navigateur. Le shell sera votre meilleur ami dans cette aventure, il vous permettra de lancer de nombreux scripts et autres sorts redoutables. 

Gandalf vous fournit les scripts pour créer un cluster managé et conforme à nos besoins dans CIVO. Pour que cela fonctionne, vous allez devoir renseigner votre APIKEY qui sera utilisé par l'outil en ligne de commande `civo`.
```shell
civo apikey save NAME APIKEY
```
On trouve APIKEY sur son compte [civo](https://www.civo.com/account/security) et NAME est le petit nom que vous voulez donner à cette connexion, si vous ne savez pas vous pouvez toujours l'appeler gitops.

Choisissez ensuite la région par défaut
```shell
civo region ls

civo region current LON1
```

## Créer le cluster
Trouvez un petit nom pour votre cluster....
```shell
export clustername=AZERTYUIOP
```


### Avec 1 seul script
Depuis le répertoire civo-k3s
```
./create-cluster.sh $clustername
```

### En commande line (pour les experts)

1) Créer un cluster 
Sans Traefik, avec 2 nodes de taille medium en métant à jours votre "KUBECONFIG" (~/.kube/config) avec cette nouvelle entrée
```shell
# Ici il faut un firewall existant nommé kube-firewall
civo k3s create ${clustername} -r=Traefik-v2-loadbalancer -r=Traefik-v2-nodeport --existing-firewall kube-firewall --size "g4s.kube.medium" --nodes 2 --wait --save --yes
```

Vérifiez que votre cluster est accessible
```shell
kubectl version
kubectl cluster-info
```

Notes :  
Un firewall est automatiquement créer, vous auriez pu en créer un que vous réutiliser. Vous pouvez aussi modifier celui créé.  
À la suppression du cluster pensez à supprimer le firewall.

1) Installer traefik
(le chemin du dossier traefik dépend de vous)
```
kubectl apply -f ./civo-k3s/traefik/ --wait
```

Vérifiez que tous vos POD dont Traefik sont up & running
```
kubectl get pod -A
```


## Récupérez l'IP du Load Balancer du cluster
Cela vous sera utile pour la suite pour accéder aux applications et siths déployées.  
Depuis le répertoire civo-k3s
```shell
source ./get-cluster-ip.sh
```

Ou si vous préférer :
```shell
export IP=$(kubectl get svc traefik-ingress-controller -n kube-system -o jsonpath='{ .status.loadBalancer.ingress[0].ip }')
echo "L'ip du load balancer est $IP"
```

Normalement **curl** sur l'IP doit renvoyer une 404 (à la fois sur le port 80 et le 443).
