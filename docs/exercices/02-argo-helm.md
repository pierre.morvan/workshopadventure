## Usage avec Helm

Le descripteur d'`Application` ArgoCD pour helm a offre une entrée spécifique `spec.source.helm`
```yaml
FIELDS:
   fileParameters	<[]Object>
     FileParameters are file parameters to the helm template

   parameters	<[]Object>
     Parameters is a list of Helm parameters which are passed to the helm
     template command upon manifest generation

   releaseName	<string>
     ReleaseName is the Helm release name to use. If omitted it will use the
     application name

   valueFiles	<[]string>
     ValuesFiles is a list of Helm value files to use when generating a template

   values	<string>
     Values specifies Helm values to be passed to helm template, typically
     defined as a block

   version	<string>
     Version is the Helm version to use for templating (either "2" or "3")

```

La surcharge des valeurs par défaut se fait en passant à helm un argument, avec en particulier `spec.source.helm.valueFiles` qui permet de passer un ou plusieurs fichiers de "value".

Et si l'on utilise des apps d'apps (ce n'est pas le cas pour l'atelier), `spec.source.helm.values` permet de surcharger les "values" https://helm.sh/docs/chart_best_practices/values/


```yaml
apiVersion: argoproj.io/v1alpha1
kind: Application
metadata:
  name: helm-dev
  namespace: argocd
spec:
  destination:
    namespace: helm-dev
    server: 'https://kubernetes.default.svc'
  source:
    path: 02-helm
    repoURL: 'https://gitlab.com/gitops-heros/deploy-sith-from-gitlab.git'
    targetRevision: main
    helm:
      valueFiles:
        - values-staging.yaml
  project: default
  syncPolicy:
    automated:
      prune: true
      selfHeal: true
    syncOptions:
      - CreateNamespace=true
```

Vous trouverez dans le repository des descripteurs à déployer les modèles prêts à emploi pour helm dans le dossier 02-helm (https://github.com/louiznk/deploy-sith/tree/main/02-helm).

Les fichiers des valeurs des environnements sont values-${env}.yaml, ce sont fichiers qui seront utilisés dans le descripteur de l'`Application` ArgoCD (`spec.source.helm.valueFiles`).


**WARNING**
Ne pas gérer les différences entre environnements via les branches, c'est un antipattern.
