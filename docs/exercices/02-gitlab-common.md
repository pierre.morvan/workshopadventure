# Ajout des jobs de deploiement

Créer un projet en important le repo https://gitlab.com/gitops-heros/deploy-sith-from-gitlab.git si ce n'est pas déjà fait.

## Configurer les "builds" du runner
Créer un fichier .gitlab-ci.yml avec pour les différents type de deploiment les stages associés.

Dans les exemples suivant, vous pouvez constater qu'il y a un '.kubeconfig' qui sera étendu par tous les déploiements qui font du deploiement dans kube.  
Ce '.kubeconfig' permet d'utiliser un container avec tous les outils utiles (kubectl, helm, kustomize, dhall en choisissant l'image de tag 1.1.0) dans lequels ont injecte la chaine de connexion au cluster (le fichier ~/.kube/config)


```yaml
.kubeconfig:
  image: louiznk/k8s-tools:1.0.0
  before_script:
  - |
    mkdir -p ${HOME}/.kube
    mv ${KUBECONFIG_FILE} ${HOME}/.kube/config

stages: 
- hello-world


hello-world:
  stage: hello-world
  extends: .kubeconfig
  script: 
    - echo "👋 Deploy dans le namespace de test"
    - kubectl create ns test 2>/dev/null || true
    - kubectl apply -n test -f ./01-static-yaml/

helm-dev:
  stage: deploy-helm-deb
  extends: .kubeconfig
  script: 
    - echo "👋 Deploy helm dans le namespace helm-dev"
    - kubectl create ns helm-dev 2>/dev/null || true
    - helm template sith-dev --create-namespace -n helm-dev -f 02-helm/values-staging.yaml --set testConnection=false 02-helm/ | kubectl apply -n helm-dev -f -


kustomize-dev:
  stage: deploy-kustomize-dev
  extends: .kubeconfig
  script: 
    - echo "👋 Deploy kustomize dans le namespace kustomize-dev"
    - kubectl create ns kustomize-dev 2>/dev/null || true
    - kustomize build 02-kustomize/overlays/staging/ | kubectl apply -n kustomize-dev -f -

## pour utiliser dhall il faut dans ".kubeconfig" utiliser l'image louiznk/k8s-tools:1.1.0
dhall-dev:
  stage: deploy-dhall-dev
  extends: .kubeconfig
  script: 
    - echo "👋 Deploy dhall dans le namespace dhall-dev"
    - kubectl create ns dhall-dev 2>/dev/null || true
    - dhall-to-yaml --documents --file 02-dhall/assembly-staging.dhall | kubectl apply -n dhall-dev -f -
```

Il manque malheureusement la boucle de contrôle pour avoir quelque chose qui réponde pleinement au besoin et s'approche des fonctionnalités d'ArgoCD mais vous n'avez pas le temps de faire cela dans les temps. Ce sera donc vous la boucle de contrôle.

Remarque : pour les ateliers pour faire au plus simple et au plus rapide nous n'utiliserons pas de branches (mais normalement il faut le faire avec des actions différentes en fonction des branches).
