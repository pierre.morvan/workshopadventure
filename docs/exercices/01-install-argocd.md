# Contexte

Argo CD est un opérateur qui va déployer des "applications" depuis leurs descripteurs versionnés dans un repo git.

Argo CD va utiliser comme kube un principe d'état désiré.
- on décrit ce que l'on veut et pas le comment
- il y a une boucle de contrôle qui fait tendre "l'application" vers son état désiré
- si par ailleurs des composants de l'application sont modifiés "à la main" il y aura soit un retour à l'état désiré, soit de la notification de la divergence

Voir https://argo-cd.readthedocs.io/en/stable/

# Installation de Argo CD dans le cluster K8S
Depuis votre env (local ou gipods).   
Il faut que vous puissiez passer des commandes kubectl. Dans le cas d'une configuration avec plusieurs clusters, cela peut être géré via le plugin konfig de kubectl (à installer si pas dans gitops).

Répertoire de base : argo

Si ce n'est pas fait faite un `export` de l'IP du load balancer
```
source ../civo-k3s/get-cluster-ip.sh
```



Installer argo-cd
```shell
./01-install_argo.sh
```

Ce script va :
- créer un namespace pour argocd.
- installer argocd.
- changer le mot de passe (attention, c'est un mot de passe de demo).
- créer une ingress route pour exposer le dashboard de ArgoCD.

Les informations de connexion au dashboard seront affichés dans le terminal par le script d'installation, soit

> https://argocd.$IP_DU_LOAD_BALANCER.sslip.io (user: admin | pwd: argodemo)
