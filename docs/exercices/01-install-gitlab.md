# Installation d'un runner

Vous allez installer un runner dans votre cluster qui fera toutes les opérations de déploiement en local.
Vous connecterez ce runner au repo contenant ce que vous voulez déployer.

## Préparation de gitlab

- La première étape consiste à créer un compte gitlab.com si ce n'est pas fait et vous connecter.  
- Créez ensuite un groupe, dans les settings de ce groupe allez sur CI/CD.  
- Puis "Expand" sur la partie "Runners"
- Désactivez les "Shared runners"
- Dans les groups runners récupérer un "registration token" que vous utiliserez par la suite (par la suite il lui sera fait référence $token)
- Créer un projet avec les descripteurs de déployements en créant un fork du projet https://gitlab.com/gitops-heros/deploy-sith-from-gitlab


## Deployer le runner
Depuis le dossier gitlab, modifier le script `01-install-runner.sh` avec l'url de votre group gitlab.


```bash
./01-install-runner.sh $token
```



## Un kubeconfig pour votre Runner
Ajouter la partie commune qui permettra à vos jobs d'accéder au cluster via l'injection à chaque build du kubeconfig.

Dans les "Settings" de gitlab, soit au niveau de votre projet "deploy-sith" soit au niveau de votre groupe (https://gitlab.com/groups/$GROUP_GITOPS/-/settings/ci_cd).  
Allez dans "Variables", cliquez sur "Expand", ajouter une variable `KUBECONFIG_FILE` de type "File". Dans cette variable, mettez le "kubeconfig" local (c'est-à-dire avec une URL d'API server interne)

exemple de KUBECONFIG_FILE

```
apiVersion: v1
clusters:
- cluster:
    certificate-authority-data: 
    TG9yZW0gaXBzdW0gZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyIGFkaXBpc2NpbmcgZWxpdCwg
    c2VkIGRvIGVpdXNtb2QgdGVtcG9yIGluY2lkaWR1bnQgdXQgbGFib3JlIGV0IGRvbG9yZSBtYWdu
    YSBhbGlxdWEuIFV0IGVuaW0gYWQgbWluaW0gdmVuaWFtLCBxdWlzIG5vc3RydWQgZXhlcmNpdGF0
    aW9uIHVsbGFtY28gbGFib3JpcyBuaXNpIHV0IGFsaXF1aXAgZXggZWEgY29tbW9kbyBjb25zZXF1
    YXQuIER1aXMgYXV0ZSBpcnVyZSBkb2xvciBpbiByZXByZWhlbmRlcml0IGluIHZvbHVwdGF0ZSB2
    ZWxpdCBlc3NlIGNpbGx1bSBkb2xvcmUgZXUgZnVnaWF0IG51bGxhIHBhcmlhdHVyLiBFeGNlcHRl
    dXIgc2ludCBvY2NhZWNhdCBjdXBpZGF0YXQgbm9uIHByb2lkZW50LCBzdW50IGluIGN1bHBhIHF1
    aSBvZmZpY2lhIGRlc2VydW50IG1vbGxpdCBhbmltIGlkIGVzdCBsYWJvcnVtLgo=
    server: https://kubernetes.default:443
  name: lorem-cluster
contexts:
- context:
    cluster: lorem-cluster
    user: lorem-cluster
  name: lorem-cluster
current-context: lorem-cluster
kind: Config
preferences: {}
users:
- name: lorem-cluster
  user:
    client-certificate-data: 
    U2VkIHV0IHBlcnNwaWNpYXRpcyB1bmRlIG9tbmlzIGlzdGUgbmF0dXMgZXJyb3Igc2l0IHZvbHVw
    dGF0ZW0gYWNjdXNhbnRpdW0gZG9sb3JlbXF1ZSBsYXVkYW50aXVtLCB0b3RhbSByZW0gYXBlcmlh
    bSwgZWFxdWUgaXBzYSBxdWFlIGFiIGlsbG8gaW52ZW50b3JlIHZlcml0YXRpcyBldCBxdWFzaSBh
    cmNoaXRlY3RvIGJlYXRhZSB2aXRhZSBkaWN0YSBzdW50IGV4cGxpY2Fiby4gTmVtbyBlbmltIGlw
    c2FtIHZvbHVwdGF0ZW0gcXVpYSB2b2x1cHRhcyBzaXQgYXNwZXJuYXR1ciBhdXQgb2RpdCBhdXQg
    ZnVnaXQsIHNlZCBxdWlhIGNvbnNlcXV1bnR1ciBtYWduaSBkb2xvcmVzIGVvcyBxdWkgcmF0aW9u
    ZSB2b2x1cHRhdGVtIHNlcXVpIG5lc2NpdW50LiBOZXF1ZSBwb3JybyBxdWlzcXVhbSBlc3QsIHF1
    aSBkb2xvcmVtIGlwc3VtIHF1aWEgZG9sb3Igc2l0IGFtZXQsIGNvbnNlY3RldHVyLCBhZGlwaXNj
    aSB2ZWxpdCwgc2VkIHF1aWEgbm9uIG51bXF1YW0gZWl1cyBtb2RpIHRlbXBvcmEgaW5jaWR1bnQg
    dXQgbGFib3JlIGV0IGRvbG9yZSBtYWduYW0gYWxpcXVhbSBxdWFlcmF0IHZvbHVwdGF0ZW0uIFV0
    IGVuaW0gYWQgbWluaW1hIHZlbmlhbSwgcXVpcyBub3N0cnVtIGV4ZXJjaXRhdGlvbmVtIHVsbGFt
    IGNvcnBvcmlzIHN1c2NpcGl0IGxhYm9yaW9zYW0sIG5pc2kgdXQgYWxpcXVpZCBleCBlYSBjb21t
    b2RpIGNvbnNlcXVhdHVyPyBRdWlzIGF1dGVtIHZlbCBldW0gaXVyZSByZXByZWhlbmRlcml0IHF1
    aSBpbiBlYSB2b2x1cHRhdGUgdmVsaXQgZXNzZSBxdWFtIG5paGlsIG1vbGVzdGlhZSBjb25zZXF1
    YXR1ciwgdmVsIGlsbHVtIHF1aSBkb2xvcmVtIGV1bSBmdWdpYXQgcXVvIHZvbHVwdGFzIG51bGxh
    IHBhcmlhdHVyPwo=
    client-key-data: 
    QXQgdmVybyBlb3MgZXQgYWNjdXNhbXVzIGV0IGl1c3RvIG9kaW8gZGlnbmlzc2ltb3MgZHVjaW11
    cyBxdWkgYmxhbmRpdGlpcyBwcmFlc2VudGl1bSB2b2x1cHRhdHVtIGRlbGVuaXRpIGF0cXVlIGNv
    cnJ1cHRpIHF1b3MgZG9sb3JlcyBldCBxdWFzIG1vbGVzdGlhcyBleGNlcHR1cmkgc2ludCBvY2Nh
    ZWNhdGkgY3VwaWRpdGF0ZSBub24gcHJvdmlkZW50LCBzaW1pbGlxdWUgc3VudCBpbiBjdWxwYSBx
    dWkgb2ZmaWNpYSBkZXNlcnVudCBtb2xsaXRpYSBhbmltaSwgaWQgZXN0IGxhYm9ydW0gZXQgZG9s
    b3J1bSBmdWdhLgo=
```


Le script `create-local-kubeconfig.sh` permet de créer ce type de fichier.

Vous pouvez aussi faire de même ainsi (hypothèse : votre kubeconfig est ~/.kube/config et n'a qu'une seul cluster).
```bash
sed "s/server:.*/server: https:\/\/kubernetes.default:443/g" ~/.kube/config
```


## Utiliser votre runner avec le kubeconfig


Comme précisé avant, assurez-vous de ne pas avoir d'autre runner (pas de shared runner) que celui de votre cluster.

Créez un descripteur de ci gitlab `.gitlab-ci.yml`

```yaml
.kubeconfig:
  image: louiznk/k8s-tools:1.0.0
  before_script:
  - |
    mkdir -p ${HOME}/.kube
    mv ${KUBECONFIG_FILE} ${HOME}/.kube/config

stages: 
- test-kubectl

hello-world:
  stage: test-kubectl
  extends: .kubeconfig
  script: 
    - echo "👋 Deploy"
    - kubectl version

```

La première partie : .kubeconfig sera utilisé par tous les `stages` qui passe des commandes kubectl | helm | kustomize. Elle permet d'utiliser le kubeconfig qui est configuré dans votre gitlab pour ce projet ou ce groupe.


Puis le stage hello-world est un simple test de la version de kubectl.


