#!/bin/bash
set -x -e

if [ "x$1" == "x" ]
then
  name="gandalf"
else
  name=$1
fi
clustername="$name-cluster"

civo kubernetes delete ${clustername} --yes

{ set +x; } 2> /dev/null # silently disable xtrace
[[ ! -f $HOME/.civo.json ]] && echo "Must initialize civo with: civo apikey save gitops <YOUR APIKEY>" && exit 1

CIVO_CURRENT_APIKEY=$(jq -r '.meta.current_apikey' $HOME/.civo.json)
if [ "$CIVO_CURRENT_APIKEY" == "" ]
then
    echo "Must initialize civo with: civo apikey save gitops <YOUR APIKEY>" && exit 1
fi

# Create if not exist kube-firewall
CIVO_APIKEY=$(jq -r ".apikeys | select(.$CIVO_CURRENT_APIKEY)" $HOME/.civo.json)
if [ "$CIVO_APIKEY" == "" ]
then
    echo "Must initialize civo with: civo apikey save gitops <YOUR APIKEY>" && exit 1
fi

CIVO_APIKEY=$(jq -r '.apikeys.gitops' $HOME/.civo.json)
CIVO_NETWORK_REGION=$(jq -r '.meta.default_region' $HOME/.civo.json)
CIVO_URL=$(jq -r '.meta.url' $HOME/.civo.json)

EXISTING_FIREWALL=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.name == "kube-firewall")')
if [ "$EXISTING_FIREWALL" != "" ]
then
    echo "🏗️ Deleting civo firewall."
    FIREWALL_ID=$(curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls?region=$CIVO_NETWORK_REGION" | jq -r '.[] | select(.name == "kube-firewall") | .id')
    curl -s --http1.1 -H "Authorization: bearer $CIVO_APIKEY" "$CIVO_URL/v2/firewalls" \
         -X DELETE "$CIVO_URL/v2/firewalls/$FIREWALL_ID?region=$CIVO_NETWORK_REGION" 2>/dev/null
fi
set -x
