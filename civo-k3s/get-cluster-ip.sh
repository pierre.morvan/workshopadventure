#!/bin/bash

IP=$(kubectl get svc traefik-ingress-controller -n kube-system -o jsonpath='{ .status.loadBalancer.ingress[0].ip }')
echo $IP
#echo $IP | xclip -sel clip
export IP=$IP
